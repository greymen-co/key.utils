<?php namespace Key\Utils\Classes;

/**
 * Class with commonly used functions that do not belong to any other helpers
 */
use \libphonenumber\PhoneNumberUtil;

class PhoneNumber
{
    public function validate($attribute, $value, $params)
    {
        $countryCode = isset($params[0]) ? $params[0] : 'NL';
        $phoneNumberUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        $phoneNumberObject = $phoneNumberUtil->parse($value, $countryCode);
        return $phoneNumberUtil->isValidNumberForRegion($phoneNumberObject,$countryCode);
    }

    public function message()
    {
        return 'The :attribute must be a phonenumber.';
    }    
}
