<?php namespace Key\Utils\Components;

use Cms\Classes\ComponentBase;
use Key\Utils\Models\Settings;
use Http;

class Recaptcha extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Recaptcha Component',
            'description' => 'Ads recaptcha hidden field to validate if you are not a robot'
        ];
    }

    public function defineProperties()
    {
        return [
            'action'    =>  [
                'title'         =>  'Recaptcha Action',
                'type'          =>  'string',
                'description'   =>  'Action to validate',
            ],

        ];
    }
    public function onRender()
    {
        $this->page['key']      = Settings::get('recaptcha_key');
        $this->page['secret']   = Settings::get('recaptcha_secret');
        $this->page['action']   = $this->property('action');
    }

    static function validate()
    {
        $token = post('recaptcha_token');
        $result = Http::post('https://www.google.com/recaptcha/api/siteverify', function($http)
        {
            $keys = SELF::getKeys();
            $http->data( 'secret',  $keys['secret'] );
            $http->data( 'response', post('recaptcha_token'));
        }); 
        
        $result = ( json_decode($result->body) );
        $return = isset($result->success) ? $result->success : false;
        return $return;
    }

    static function getKeys()
    {
        $keys['key']     = Settings::get('recaptcha_key');
        $keys['secret']  = Settings::get('recaptcha_secret');
        return $keys;
    }
}
