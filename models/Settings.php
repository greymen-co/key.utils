<?php namespace Key\Utils\Models;

use Model;
use Lang;

/**
 * Settings Model
 */
class Settings extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'key_utils_settings';

    public $settingsFields = 'fields.yaml';

    // protected $casts = [
    //     'default_offset_x' => 'integer',
    //     'default_offset_y' => 'integer',
    //     'default_quality'  => 'integer',
    //     'default_sharpen'  => 'integer'
    // ];

    public $rules = [
        // 'default_quality'           => 'integer|between:0,100',
        // 'default_sharpen'           => 'integer|between:0,100',
        // 'tinypng_developer_key'     => 'required_if:enable_tinypng,1'
    ];

    public $customMessages = [];

    public function __construct(){
        // $this->customMessages['valid_tinypng_key'] = Lang::get('Key.utils::lang.settings.tinypng_invalid_key');
        parent::__construct();
    }
    public function afterSave()
    {
        $values = $this->toArray()['value']; 
        if (isset ($values['css_language_button']))
        {
            file_put_contents(plugins_path().'/key/utils/assets/css/css_language_button.css',$values['css_language_button']);
        }
    }
    public function beforeValidate()
    {
        // if ($this->enable_tinypng == 1) {
        //     $this->rules['tinypng_developer_key'] .= '|valid_tinypng_key';
        // }
    }

    // Default setting data
    public function initSettingsData()
    {
        $this->default_use_repeater_auto_fold = 1;
    }
}
