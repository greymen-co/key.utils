<?php namespace Key\Utils\Traits;

use Flash;
use Redirect;
use Event;
use Settings;
use Cms\Classes\Theme;
use BackendAuth;
use Backend\Classes\ControllerBehavior;
use Backend\Widgets\Toolbar;
use Cms\Models\ThemeLog;
use Exception;
use Log;

trait Loggable
{
    // public $onlyLoggedInUsers = true;

    public static function bootLoggable()
    {
    }
    public function initializeLoggable()
    {
        $this->bindEvent('model.afterUpdate', function () {
            $this->loggableAfter($this);
        });
        $this->bindEvent('model.afterCreate', function () {
            $this->loggableAfter($this, ThemeLog::TYPE_CREATE);
        });
        $this->bindEvent('model.afterDelete', function () {
            $this->loggableAfter($this, ThemeLog::TYPE_DELETE);
        });
    }

    protected function getLoggedOutUsers()
    {
        return isset($this->loggedOutUsers) ? $this->loggedOutUsers : false;

    }
    public function loggableAfter($model, $type = ThemeLog::TYPE_UPDATE)
    {
        // dd($this->getOnlyLoggedInUsers());
        if ($this->getLoggedOutUsers() || BackendAuth::getUser()) {
            $record                 = new ThemeLog;
            $record->type           = $type;
            $record->user_id        = BackendAuth::getUser() ? BackendAuth::getUser()->id : null;
            $record->theme          = Theme::getEditThemeCode();
            $record->template       = get_class($this);
            $record->old_template   = get_class($this);
//            $record->old_template   = '[model]';
            $record->content        = json_encode($model->attributes,JSON_PRETTY_PRINT);
            $record->old_content    = json_encode($model->original,JSON_PRETTY_PRINT);
    
            try {
                $record->save();
            }
            catch (Exception $ex) {
                Log::error($ex->getMessage());
            }
    
            return $record;    
        }

    }
}


